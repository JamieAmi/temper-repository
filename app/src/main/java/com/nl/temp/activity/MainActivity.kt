package com.nl.temp.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.nl.temp.R
import com.nl.temp.adapter.JobsAdapter
import com.nl.temp.api.ApiErrorHandler
import com.nl.temp.model.Jobs
import com.nl.temp.viewmodel.JobListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_action_bar.*
import kotlinx.android.synthetic.main.content_job_list.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.core.parameter.parametersOf
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private lateinit var jobListViewModel: JobListViewModel
    private lateinit var jobsAdapter: JobsAdapter
    private lateinit var jobs:ArrayList<Jobs>
    private val apiErrorHandler by inject<ApiErrorHandler> { parametersOf(this) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        jobListViewModel = getViewModel<JobListViewModel>()
        jobs = ArrayList<Jobs>()
        setupUI()
    }

    private fun setupUI() {
        jobListViewModel.setSelectedDate(Date())
        jobListViewModel.getJobList(Date())
        jobsAdapter = JobsAdapter(this, jobs)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvJobs.layoutManager = linearLayoutManager
        rvJobs.adapter = jobsAdapter
        jobListViewModel.dayString.observe(this, androidx.lifecycle.Observer { dateString ->
            tvDate.text = dateString.toString()
        })
        jobListViewModel.jobsList.observe(this, androidx.lifecycle.Observer { list ->
            jobs.clear()
            jobs.addAll(list)
            jobsAdapter.notifyDataSetChanged()
           if( srlJobs.isRefreshing){
               srlJobs.isRefreshing = false
           }

        })
        jobListViewModel.apiError.observe(this, androidx.lifecycle.Observer {
            apiErrorHandler.handleError(it,clMain)
        })
        srlJobs.setOnRefreshListener {
            jobListViewModel.getJobList(Date())
        }
        btnLogin.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
        }
        btnSubscribe.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
        }
    }
}