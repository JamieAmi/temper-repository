package com.nl.temp.di



import com.google.gson.Gson
import com.nl.temp.api.ApiErrorHandler
import com.nl.temp.api.ApiManager
import com.nl.temp.usecase.GetJobListUsecase
import com.nl.temp.viewmodel.JobListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { JobListViewModel(get()
    ) }

}
val restModule = module {
    single { ApiManager( ) }
}

val errorHandlerModule = module {
    single {
        ApiErrorHandler(get())
    }
}

val jsonparserModule = module{
    single{Gson()}
}

val useacseModule = module {
    factory { GetJobListUsecase(get(),get() )}

}

val appModules = listOf(viewModelModule, restModule, errorHandlerModule,
    jsonparserModule,
    useacseModule)

