package com.nl.temp.usecase

import com.nl.temp.model.Result

abstract class BaseUsecase<Q:BaseUsecase.RequestValues> {
    var requestValues:Q? = null
    interface RequestValues
    abstract suspend fun executeUsecase(requestValues:Q?):Result

}