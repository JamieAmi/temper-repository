package com.nl.temp.usecase

import com.android.volley.Response
import com.google.gson.Gson
import com.nl.temp.api.ApiManager
import com.nl.temp.model.ApiResponse
import com.nl.temp.model.Result
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class GetJobListUsecase(private val apiManager: ApiManager,private val gson: Gson):BaseUsecase<GetJobListUsecase.RequestValues>() {
    class RequestValues(val date: String) : BaseUsecase.RequestValues

    override suspend fun executeUsecase(requestValues:RequestValues?) = suspendCoroutine<Result> { cont->
        apiManager.getJobs(Response.Listener {
            val response:ApiResponse = gson.fromJson(it.toString(),ApiResponse::class.java)
            cont.resume(Result.Success(response.jobsList))
        }, Response.ErrorListener {
            cont.resume(Result.Failure(it))
        },
        requestValues!!.date)
    }
}