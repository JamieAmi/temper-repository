package com.nl.temp.api


import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject



class ApiManager() {
    fun getJobs(
        getJobsSuccessListener: Response.Listener<JSONObject>,
        getJobsErrorListener: Response.ErrorListener,date:String
    ) {
        var url = UrlConstatants.SHIFTS.replace("{date}",date)
        val queue = VolleySingleton.getRequestQueue()
        val request = object : JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            getJobsSuccessListener,
            getJobsErrorListener
        ) {
            override fun getHeaders(): MutableMap<String, String> {
                return super.getHeaders()
            }
        }
        request.retryPolicy = DefaultRetryPolicy(
            20000,
            0,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        queue.add(request)
    }


}