package com.nl.temp.api

import android.graphics.Bitmap
import android.util.LruCache
import com.android.volley.toolbox.ImageLoader

class BitmapLruCache(maxSize : Int) : LruCache<String, Bitmap>(maxSize), ImageLoader.ImageCache {

    override fun getBitmap(url: String?): Bitmap {
        return get(url)
    }

    override fun putBitmap(url: String?, bitmap: Bitmap?) {
        put(url,bitmap)
    }

    override fun sizeOf(key: String?, value: Bitmap?): Int {
        return value!!.rowBytes * value.height
    }

}