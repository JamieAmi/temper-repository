package com.nl.temp.api

import com.nl.temp.R


object UrlConstatants {

    val BASE = "https://temper.works/api/v3/"
    val SHIFTS = BASE+"shifts?filter[date]={date}"

}