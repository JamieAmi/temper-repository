package com.nl.temp.api

import android.content.Context
import androidx.coordinatorlayout.widget.CoordinatorLayout

import com.android.volley.AuthFailureError
import com.android.volley.VolleyError

import com.nl.temp.R
import com.nl.temp.util.SnackMessageCreator



class ApiErrorHandler(val context: Context) {

    fun handleError(it: VolleyError?, view: CoordinatorLayout?) {
        if (it is AuthFailureError) {
            SnackMessageCreator.createSnackBar(
                context.getString(R.string.error_session),
                view!!, context, R.color.colorErrorRed
            )
        } else {
            val response = it!!.networkResponse
            if (response?.data != null && response.statusCode != null) {
                var errorMessage = context.getString(R.string.api_error_internal_server)
                when (response.statusCode) {
                    422 -> {
                        errorMessage = context.getString(R.string.api_error_invalid_date)
                    }
                    500 -> {
                        errorMessage = context.getString(R.string.api_error_invalid_date)
                    }
                    else -> {
                        errorMessage
                    }
                }
                SnackMessageCreator.createSnackBar(
                    errorMessage, view!!, context, R.color.colorErrorRed
                )
            } else {
                SnackMessageCreator.createSnackBar(
                    context.getString(R.string.api_error_internal_server),
                    view!!,
                    context,
                    R.color.colorErrorRed
                )
            }
        }
    }
}