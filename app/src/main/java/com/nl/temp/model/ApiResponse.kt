package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ApiResponse {
    @SerializedName("data")
    @Expose
    var jobsList: List<Jobs>? = null

    @SerializedName("aggregations")
    @Expose
    var aggregations: Aggregations? = null
}