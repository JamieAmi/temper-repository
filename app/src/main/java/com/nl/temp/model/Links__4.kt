package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Links__4 {
    @SerializedName("get_directions")
    @Expose
    var getDirections: String? = null
}