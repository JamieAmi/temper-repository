package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Aggregations {
    @SerializedName("count")
    @Expose
    var count: Int? = null
}