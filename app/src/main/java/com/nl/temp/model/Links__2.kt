package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Links__2 {
    @SerializedName("hero_image")
    @Expose
    var heroImage: String? = null

    @SerializedName("thumb_image")
    @Expose
    var thumbImage: String? = null
}