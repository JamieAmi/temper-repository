package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Links__3 {
    @SerializedName("icon")
    @Expose
    var icon: String? = null
}