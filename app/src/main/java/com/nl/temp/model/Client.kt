package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Client {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("slug")
    @Expose
    var slug: String? = null

    @SerializedName("registration_name")
    @Expose
    var registrationName: String? = null

    @SerializedName("registration_id")
    @Expose
    var registrationId: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("allow_temper_trial")
    @Expose
    var allowTemperTrial: Boolean? = null

    @SerializedName("blocked_minutes_before_shift")
    @Expose
    var blockedMinutesBeforeShift: Any? = null

    @SerializedName("links")
    @Expose
    var links: Links__2? = null

    @SerializedName("rating")
    @Expose
    var rating: Rating? = null

    @SerializedName("average_response_time")
    @Expose
    var averageResponseTime: Any? = null

    @SerializedName("factoring_allowed")
    @Expose
    var factoringAllowed: Boolean? = null

    @SerializedName("factoring_payment_term_in_days")
    @Expose
    var factoringPaymentTermInDays: Int? = null
}