package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NameTranslation {
    @SerializedName("en_GB")
    @Expose
    var enGB: String? = null

    @SerializedName("nl_NL")
    @Expose
    var nlNL: String? = null
}