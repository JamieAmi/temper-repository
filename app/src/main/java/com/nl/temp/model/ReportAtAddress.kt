package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReportAtAddress {
    @SerializedName("zip_code")
    @Expose
    var zipCode: String? = null

    @SerializedName("street")
    @Expose
    var street: String? = null

    @SerializedName("number")
    @Expose
    var number: String? = null

    @SerializedName("number_with_extra")
    @Expose
    var numberWithExtra: String? = null

    @SerializedName("extra")
    @Expose
    var extra: String? = null

    @SerializedName("city")
    @Expose
    var city: String? = null

    @SerializedName("line1")
    @Expose
    var line1: String? = null

    @SerializedName("line2")
    @Expose
    var line2: String? = null

    @SerializedName("links")
    @Expose
    var links: Links__4? = null

    @SerializedName("country")
    @Expose
    var country: Country? = null

    @SerializedName("geo")
    @Expose
    var geo: Geo? = null

    @SerializedName("region")
    @Expose
    var region: String? = null
}