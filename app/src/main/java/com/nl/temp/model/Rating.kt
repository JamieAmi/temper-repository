package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Rating {
    @SerializedName("count")
    @Expose
    var count: Int? = null

    @SerializedName("average")
    @Expose
    var average: Any? = null
}