package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class JobLink {
    @SerializedName("hero_380_image")
    @Expose
    var hero380Image: String? = null

    @SerializedName("self")
    @Expose
    var self: String? = null

    @SerializedName("client")
    @Expose
    var client: String? = null

    @SerializedName("report_at_address")
    @Expose
    var reportAtAddress: String? = null

    @SerializedName("job_category")
    @Expose
    var jobCategory: String? = null

    @SerializedName("skills")
    @Expose
    var skills: String? = null

    @SerializedName("appearances")
    @Expose
    var appearances: String? = null
}