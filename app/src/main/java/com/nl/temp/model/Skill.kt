package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Skill {
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("id")
    @Expose
    var id: String? = null
}