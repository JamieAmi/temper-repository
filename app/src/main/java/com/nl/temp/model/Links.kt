package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Links {
    @SerializedName("self")
    @Expose
    var self: String? = null

    @SerializedName("match_aggregates")
    @Expose
    var matchAggregates: String? = null

    @SerializedName("job")
    @Expose
    var job: String? = null
}