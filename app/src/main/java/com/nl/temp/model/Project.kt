package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Project {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("archived_at")
    @Expose
    var archivedAt: Any? = null

    @SerializedName("client")
    @Expose
    var client: Client? = null
}