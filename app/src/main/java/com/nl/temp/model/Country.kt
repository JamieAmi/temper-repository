package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Country {
    @SerializedName("iso3166_1")
    @Expose
    var iso31661: String? = null

    @SerializedName("human")
    @Expose
    var human: String? = null
}