package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Category {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("internalId")
    @Expose
    var internalId: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("name_translation")
    @Expose
    var nameTranslation: NameTranslation? = null

    @SerializedName("slug")
    @Expose
    var slug: String? = null

    @SerializedName("links")
    @Expose
    var links: Links__3? = null
}