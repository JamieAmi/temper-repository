package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Jobs {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("starts_at")
    @Expose
    var startsAt: String? = null

    @SerializedName("ends_at")
    @Expose
    var endsAt: String? = null

    @SerializedName("duration")
    @Expose
    var duration: Int? = null

    @SerializedName("tempers_needed")
    @Expose
    var tempersNeeded: Int? = null

    @SerializedName("enable_auto_accept_recent_freelancers")
    @Expose
    var enableAutoAcceptRecentFreelancers: Boolean? = null

    @SerializedName("cancellation_policy")
    @Expose
    var cancellationPolicy: Int? = null

    @SerializedName("archived_at")
    @Expose
    var archivedAt: Any? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("earnings_per_hour")
    @Expose
    var earningsPerHour: EarningsPerHour? = null

    @SerializedName("variable_pricing")
    @Expose
    var variablePricing: Boolean? = null

    @SerializedName("factoring_allowed")
    @Expose
    var factoringAllowed: Boolean? = null

    @SerializedName("is_flexible")
    @Expose
    var isFlexible: Boolean? = null

    @SerializedName("time_variation_message")
    @Expose
    var timeVariationMessage: String? = null

    @SerializedName("start_time_earlier_variation")
    @Expose
    var startTimeEarlierVariation: Int? = null

    @SerializedName("start_time_later_variation")
    @Expose
    var startTimeLaterVariation: Int? = null

    @SerializedName("end_time_earlier_variation")
    @Expose
    var endTimeEarlierVariation: Int? = null

    @SerializedName("end_time_later_variation")
    @Expose
    var endTimeLaterVariation: Int? = null

    @SerializedName("earliest_possible_start_time")
    @Expose
    var earliestPossibleStartTime: String? = null

    @SerializedName("latest_possible_end_time")
    @Expose
    var latestPossibleEndTime: String? = null

    @SerializedName("links")
    @Expose
    var links: Links? = null

    @SerializedName("open_positions")
    @Expose
    var openPositions: Int? = null

    @SerializedName("high_chance_score")
    @Expose
    var highChanceScore: Int? = null

    @SerializedName("applications_count")
    @Expose
    var applicationsCount: Int? = null

    @SerializedName("flexpools")
    @Expose
    var flexpools: List<Flexpool>? = null

    @SerializedName("job")
    @Expose
    var job: Job? = null

    @SerializedName("recurring_shift_schedule")
    @Expose
    var recurringShiftSchedule: Any? = null

    @SerializedName("has_substituted_openings")
    @Expose
    var hasSubstitutedOpenings: Boolean? = null

    @SerializedName("distance")
    @Expose
    var distance: Any? = null

    @SerializedName("is_auto_accepted_when_applied")
    @Expose
    var isAutoAcceptedWhenApplied: Boolean? = null

    @SerializedName("chance_of_success")
    @Expose
    var chanceOfSuccess: Boolean? = null

    @SerializedName("average_estimated_earnings_per_hour")
    @Expose
    var averageEstimatedEarningsPerHour: AverageEstimatedEarningsPerHour? = null
}