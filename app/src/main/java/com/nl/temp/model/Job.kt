package com.nl.temp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Job {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("is_agency")
    @Expose
    var isAgency: Boolean? = null

    @SerializedName("extra_briefing")
    @Expose
    var extraBriefing: String? = null

    @SerializedName("dress_code")
    @Expose
    var dressCode: String? = null

    @SerializedName("tips")
    @Expose
    var tips: Boolean? = null

    @SerializedName("slug")
    @Expose
    var slug: String? = null

    @SerializedName("reference")
    @Expose
    var reference: Any? = null

    @SerializedName("archived_at")
    @Expose
    var archivedAt: Any? = null

    @SerializedName("links")
    @Expose
    var links: JobLink? = null

    @SerializedName("skills")
    @Expose
    var skills: List<Skill>? = null

    @SerializedName("project")
    @Expose
    var project: Project? = null

    @SerializedName("category")
    @Expose
    var category: Category? = null

    @SerializedName("report_to")
    @Expose
    var reportTo: ReportTo? = null

    @SerializedName("appearances")
    @Expose
    var appearances: List<Appearance>? = null

    @SerializedName("report_at_address")
    @Expose
    var reportAtAddress: ReportAtAddress? = null
}