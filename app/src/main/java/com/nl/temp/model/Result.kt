package com.nl.temp.model

import com.android.volley.VolleyError

sealed class Result {
    class Success(val response: List<Jobs>?) : Result()
    class Failure(val error: VolleyError) : Result()

}