package com.nl.temp

import android.app.Application
import android.content.Context

import com.nl.temp.di.appModules
import com.nl.temp.api.VolleySingleton
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TemperApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        VolleySingleton.init(this)
        startKoin {
            androidLogger()
            androidContext(applicationContext)
            koin.loadModules(appModules)

        }

    }

    init {
        instance = this
    }

    companion object {
        private var instance: TemperApplication? = null

        fun getApp(): Context {
            return instance!!.applicationContext
        }
    }
}