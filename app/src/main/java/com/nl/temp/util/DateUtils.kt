package com.nl.temp.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    private var dateFormat1 =
        SimpleDateFormat("EEEE dd MMMM")
    private var dateFormat2 =
        SimpleDateFormat("yyyy-MM-dd")
    private var dateTimeFormat1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
    private var timeFormat1 = SimpleDateFormat("HH:mm")

    @JvmStatic
    fun getDateStringByDate(date: Date) : String {
        return dateFormat2.format(date)
    }
    @JvmStatic
    fun getDayAndDateByDate(date: Date) : String {
        return dateFormat1 .format(date)
    }
    @JvmStatic
    fun getLocalTimeByUtcSateTime(dateTime: String) : String {
        dateTimeFormat1.timeZone = TimeZone.getTimeZone("UTC")
        val date: Date = dateTimeFormat1 .parse(dateTime)
        timeFormat1.timeZone = TimeZone.getDefault()
        return timeFormat1.format(date).toString()
    }
}