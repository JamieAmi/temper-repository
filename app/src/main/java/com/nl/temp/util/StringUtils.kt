package com.nl.temp.util

import java.text.DecimalFormat
import java.util.*

object StringUtils {

    fun formatAmountForCurrency(number: Double?):String{
        val dec = DecimalFormat("#.00")
        return dec.format(number)
    }

    fun getCurrencySymbol(currency: String?):String{
        val curr: Currency = Currency.getInstance(currency)
        return curr.symbol.toString()
    }
}