package com.nl.temp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nl.temp.databinding.CellJobBinding
import com.nl.temp.model.Jobs
import com.nl.temp.util.DateUtils
import com.nl.temp.util.StringUtils
import com.squareup.picasso.Picasso

class JobsAdapter(private val context: Context,private val items:ArrayList<Jobs>) : RecyclerView.Adapter<JobsAdapter.JobViewHolder>() {
    inner class JobViewHolder(val binding: CellJobBinding): RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHolder {
        val binding = CellJobBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return JobViewHolder(binding)
    }

    override fun onBindViewHolder(holder: JobViewHolder, position: Int) {
        val item = items.get(position)
        Picasso.get().load(item.job?.project?.client?.links?.heroImage).into(holder.binding.ivJob)
        holder.binding.tvHourlyRate.text = StringUtils.getCurrencySymbol(item.earningsPerHour?.currency)+" "+StringUtils.formatAmountForCurrency(item.earningsPerHour?.amount)
        holder.binding.tvJobTile.text = item.job?.title.toString().toUpperCase()
        holder.binding.tvClientName.text = item.job?.project?.client?.name.toString()
        var startTime = ""
        if(item.startsAt!= null){
            startTime  = DateUtils.getLocalTimeByUtcSateTime(item.startsAt!!)
        }
        var endTime = ""
        if(item.endsAt!= null){
            endTime = DateUtils.getLocalTimeByUtcSateTime(item.endsAt !!)
        }
        holder.binding.tvShiftTime.text = "$startTime - $endTime"
    }

    override fun getItemCount(): Int {
       return items.size
    }
}