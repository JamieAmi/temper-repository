package com.nl.temp.viewmodel


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.volley.VolleyError
import com.nl.temp.model.Jobs
import com.nl.temp.model.Result
import com.nl.temp.usecase.GetJobListUsecase
import com.nl.temp.util.DateUtils

import kotlinx.coroutines.launch
import java.util.*


class JobListViewModel(private val getJobListUsecase: GetJobListUsecase):ViewModel(){
    var dayString:MutableLiveData<String> = MutableLiveData()
    var jobsList:MutableLiveData<ArrayList<Jobs>> = MutableLiveData()
    var apiError:MutableLiveData<VolleyError> = MutableLiveData()

    fun setSelectedDate(date: Date){
        dayString.value = DateUtils.getDayAndDateByDate(date)
    }
    fun getJobList(date: Date){
        val dateString = DateUtils.getDateStringByDate(date)
        viewModelScope.launch {
            val result = getJobListUsecase.executeUsecase(GetJobListUsecase.RequestValues(dateString))
            when (result){
                is Result.Success -> {
                    jobsList.postValue(result.response as ArrayList<Jobs>?)
                }
                is Result.Failure -> {
                    apiError.postValue(result.error)
                }
            }
        }
    }
}