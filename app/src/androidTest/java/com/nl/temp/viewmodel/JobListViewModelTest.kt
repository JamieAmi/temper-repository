package com.nl.temp.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4




import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.nl.temp.api.ApiManager
import com.nl.temp.api.VolleySingleton
import com.nl.temp.model.Jobs
import com.nl.temp.usecase.GetJobListUsecase
import com.nl.temp.util.DateUtils


import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*
import kotlin.collections.ArrayList



@RunWith(AndroidJUnit4::class)

class JobListViewModelTest{

    @get:Rule
    public val instantExecutorRule = InstantTaskExecutorRule()
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var jobListViewModel: JobListViewModel
    @Mock
    private lateinit var apiManager: ApiManager
    private val gson = Gson()
    @Mock
    private lateinit var getJobListUsecase :GetJobListUsecase
    @Mock
    private lateinit var mockApplicationContext: Context
    @Mock
    private lateinit var jobListObserver:Observer<ArrayList<Jobs>>
    @Mock
    private lateinit var apiErrorObserver:Observer<VolleyError>


    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mockApplicationContext  = Mockito.mock(Context::class.java)
        VolleySingleton.init(mockApplicationContext)
        apiManager = ApiManager()
        getJobListUsecase = GetJobListUsecase(apiManager, gson)
        jobListViewModel = JobListViewModel(getJobListUsecase)
    }

    @Test
    fun testGetJobsList_returnList(){
        var jobs:ArrayList<Jobs> = ArrayList()
    val dateString = DateUtils.getDateStringByDate(Date())
        testCoroutineRule.runBlockingTest {
            Mockito.doReturn(jobs).`when`(getJobListUsecase).executeUsecase(GetJobListUsecase.RequestValues(dateString))
            jobListViewModel.jobsList.observeForever(jobListObserver);
            Mockito.verify(getJobListUsecase).executeUsecase(GetJobListUsecase.RequestValues(dateString))
           Mockito.verify(jobListObserver).onChanged(jobs)
            jobListViewModel.jobsList.removeObserver(jobListObserver)
        }
    }
    @Test
    fun testGetJobsList_servereror_returnVolleyError(){
        var error = VolleyError()
        val dateString = "2021-08-00"
        testCoroutineRule.runBlockingTest {
            Mockito.doReturn(error).`when`(getJobListUsecase).executeUsecase(GetJobListUsecase.RequestValues(dateString))
            jobListViewModel.apiError.observeForever(apiErrorObserver);
            Mockito.verify(getJobListUsecase).executeUsecase(GetJobListUsecase.RequestValues(dateString))
            Mockito.verify(apiErrorObserver).onChanged(error)
            jobListViewModel.apiError.removeObserver(apiErrorObserver)
        }
    }

}


